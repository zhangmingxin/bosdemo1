package com.itheima.web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Ajax的get方式的请求
 */
public class GetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		String name = request.getParameter("");
		//response.getWriter().println("Hello AJAX GET 请求方式... ");
		response.setContentType("text/html;charset=UTF-8");
		String name = request.getParameter("name");
		String value = request.getParameter("value");
		response.setContentType("text/html;charset=UTF-8");
		String str=new String(name.getBytes("ISO-8859-1"),"UTF-8");
		response.getWriter().println("AJAX请求带参数的方法name="+str+",value="+value);
		//System.out.println("jjj..................");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
