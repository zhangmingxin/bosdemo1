  基于java实现的异步删除数据库中的数据（根据ID删除）

HTML页面---JS代码：
	function doDelete(){
				//得到勾选行,返回一个数组
				var rows = $("#grid").datagrid("getSelections");
				var ids="";
				//用逗号分隔每条记录的ＩＤ
				for(var i=0;i<rows.length;i++){
				 	ids+=rows[i].id+",";
				}
				 //去掉最后一个逗号(如果不需要去掉，就不用写)
			    if (ids.length > 0) {
			        ids = ids.substr(0, ids.length - 1);
			    }
				//得到勾选行的属性对象
				//var data = rows[0];
				$.messager.confirm('确认','您确认想要删除记录吗？',function(flag){    
				   	if (flag){    
				        //确定删除
						$.post("../../fixed_area_delBatch.action",{ids:ids},function(data){
							if(data.flag){
								//删除成功
								$.messager.alert("标题","删除成功！","info",function(){
									//重新加载数据 表格datagrid,分页查询,页面保持在当前页
									$('#grid').datagrid("reload");
								});
							}else{
								//删除失败，并显示提示信息
								alert(data.msg);
							}
						});
						
				   	}
				}); 		
			}
			
	//工具栏,（不理解的话，可以查看easyUI的API 网址：http://www.jeasyui.net/）
			var toolbar = [{
				id : 'button-delete',
				text : '删除',
				iconCls : 'icon-cancel',
				handler : doDelete
			}];
			
	$(function(){
				// 先将body隐藏，再显示，不会出现页面刷新效果
				$("body").css({visibility:"visible"});
				
				// 定区数据表格
				$('#grid').datagrid( {
					iconCls : 'icon-forward',
					fit : true,
					border : true,
					rownumbers : true,
					striped : true,
					pageList: [30,50,100],
					pagination : true,
					toolbar : toolbar,
					url : "../../fixed_area_pageQuery.action",
					idField : 'id',
					columns : columns,
					onDblClickRow : doDblClickRow
				});
	});

	
JAVA---Action代码：
	//删除数据：
	@Action(value="fixed_area_delBatch",results={@Result(name="success",results={@Result(name="success",type="json")})
	public String delBatch(){
		//自定义ResultMsg，用于保存查询结果
		ResultMsg result=new ResultMsg();
		
		boolean flag = fixedAreaService.delBatch(ids);
		if(flag==true){
			result.setMsg("删除成功");
			result.setFlag(true);
		}else{
			result.setMsg("删除的内容不存在！");
			result.setFlag(false);
		}
		//将结果信息存入值栈
		ActionContext.getContext().getValueStack().push(result);
		return SUCCESS;
	}
JAVA--Service代码：
	//批量删除
	@Override
	public boolean delBatch(String ids) {
		boolean flag=true;
		
		String[] str = ids.split(",");
		
		for (String id : str) {
			//判断相应ID的fixedArea是否存在,
			FixedArea fixedArea = fixedAreaRepository.findOne(id);
			
			if(fixedArea!=null){
				//存在，删除
				fixedAreaRepository.delete(id);
			}else{
				//不存在
				flag=false;
			}
		}
		return flag;
	}
JAVA--DAO代码
	//Spring data jpa 操作数据库持久层
	public interface FixedAreaRepository extends JpaRepository<FixedArea, String>,JpaSpecificationExecutor<FixedArea>{

	}
	
	
	注意：其中涉及到easyUI的东西，eg:datagrid,可查看easyUI的API 网址：http://www.jeasyui.net/）  进行理解